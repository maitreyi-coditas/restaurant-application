"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExcludedPath = exports.authorize = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
const authorize = (excludedPaths) => {
    return (req, res, next) => {
        var _a;
        try {
            if (excludedPaths.find(e => e.path === req.url && e.method === req.method)) {
                return next();
            }
            const token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(' ')[1];
            if (!token)
                return next({ message: 'UNAUTHORIZED', statusCode: 401 });
            const { JWT_SECRET } = process.env;
            (0, jsonwebtoken_1.verify)(token, JWT_SECRET || '');
            next();
        }
        catch (e) {
            next({ message: 'UNAUTHORIZED', statusCode: 401 });
        }
    };
};
exports.authorize = authorize;
class ExcludedPath {
    constructor(path, method) {
        this.path = path;
        this.method = method;
    }
}
exports.ExcludedPath = ExcludedPath;
//# sourceMappingURL=authorize.js.map