"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adminList = void 0;
const role_types_1 = require("../feature-modules/roles/role.types");
exports.adminList = [
    {
        name: 'Administrator-1',
        email: "admin1@gmail.com",
        password: "adminPassword@1",
        role: role_types_1.ROLE.ADMIN
    },
    {
        name: 'Administrator-2',
        email: "admin2@gmail.com",
        password: "adminPassword@2",
        role: role_types_1.ROLE.ADMIN
    },
    {
        name: 'Administrator-3',
        email: "admin3@gmail.com",
        password: "adminPassword@3",
        role: role_types_1.ROLE.ADMIN
    }
];
//# sourceMappingURL=populate-admin.js.map