"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticateOwner = void 0;
const restaurant_services_1 = __importDefault(require("../feature-modules/restaurant/restaurant.services"));
const user_responses_1 = require("../feature-modules/user/user.responses");
const authenticateOwner = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = res.locals.tokenDecode;
    const { restaurantId } = req.params;
    const restaurant = yield restaurant_services_1.default.findOne({ _id: restaurantId });
    if ((restaurant === null || restaurant === void 0 ? void 0 : restaurant.ownerId.toString()) === id.toString()) {
        return next();
    }
    throw user_responses_1.USER_RESPONSE.UNAUTHORISED_ACCESS;
});
exports.authenticateOwner = authenticateOwner;
//# sourceMappingURL=authenticateOwner.js.map