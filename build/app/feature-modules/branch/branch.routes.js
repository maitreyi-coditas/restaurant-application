"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const permit_1 = require("../../middleware/permit");
const role_types_1 = require("../roles/role.types");
const branch_services_1 = __importDefault(require("./branch.services"));
const response_handler_1 = require("../../utility/response.handler");
const router = (0, express_1.Router)();
router.post('/create-branch/:parentRestaurantId', (0, permit_1.permit)([role_types_1.ROLE.OWNER]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { parentRestaurantId } = req.params;
        const { id } = res.locals.tokenDecode;
        const branch = req.body;
        const result = yield branch_services_1.default.create(parentRestaurantId, id, branch);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get('/', (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield branch_services_1.default.findAll();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
exports.default = router;
//# sourceMappingURL=branch.routes.js.map