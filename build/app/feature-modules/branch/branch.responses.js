"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BRANCH_RESPONSE = void 0;
exports.BRANCH_RESPONSE = {
    NOT_CREATED: {
        statusCode: 500,
        message: "Branch not created"
    }
};
//# sourceMappingURL=branch.responses.js.map