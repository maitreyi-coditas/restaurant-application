"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const restaurant_services_1 = __importDefault(require("../restaurant/restaurant.services"));
const resturant_responses_1 = require("../restaurant/resturant.responses");
const user_responses_1 = require("../user/user.responses");
const branch_repo_1 = __importDefault(require("./branch.repo"));
const branch_responses_1 = require("./branch.responses");
const create = (restaurantId, id, branch) => __awaiter(void 0, void 0, void 0, function* () {
    const restaurant = yield restaurant_services_1.default.findOne({ _id: restaurantId });
    if (!restaurant)
        throw resturant_responses_1.RESTAURANT_RESPONSE.NOT_FOUND;
    if ((restaurant === null || restaurant === void 0 ? void 0 : restaurant.ownerId.toString()) === id.toString()) {
        branch.ownerId = restaurant.ownerId;
        branch.parentRestaurantId = restaurant._id;
        const newBranch = yield branch_repo_1.default.create(branch);
        if (!newBranch)
            throw branch_responses_1.BRANCH_RESPONSE.NOT_CREATED;
        const updatedRestaurantResponse = yield restaurant_services_1.default.addBranch({ _id: restaurant._id }, newBranch);
        return updatedRestaurantResponse;
    }
    else {
        throw user_responses_1.USER_RESPONSE.UNAUTHORISED_ACCESS;
    }
});
const findAll = () => branch_repo_1.default.findAll();
const updateStatus = (filter, status) => __awaiter(void 0, void 0, void 0, function* () { return branch_repo_1.default.updateStatus(filter, status); });
exports.default = {
    create,
    findAll,
    updateStatus
};
//# sourceMappingURL=branch.services.js.map