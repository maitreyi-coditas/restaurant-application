"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const branch_schema_1 = require("./branch.schema");
const create = (branch) => branch_schema_1.BranchModel.create(branch);
const findAll = () => branch_schema_1.BranchModel.find();
const updateStatus = (filter, status) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield branch_schema_1.BranchModel.updateMany(filter, status);
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to update the status of the restaurant" };
    }
});
exports.default = {
    create,
    findAll,
    updateStatus
};
//# sourceMappingURL=branch.repo.js.map