"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BranchModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base-schema");
const branchSchema = new base_schema_1.BaseSchema({
    location: {
        type: String,
        required: true
    },
    menu: {
        type: [
            {
                name: {
                    type: String
                },
                price: {
                    type: String
                }
            }
        ],
        required: true
    },
    ownerId: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
    parentRestaurantId: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true,
        ref: "Restaurant"
    }
});
exports.BranchModel = (0, mongoose_1.model)("BranchRestaurants", branchSchema);
//# sourceMappingURL=branch.schema.js.map