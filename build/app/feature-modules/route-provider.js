"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_routes_1 = __importDefault(require("./user/user.routes"));
const role_routes_1 = __importDefault(require("./roles/role.routes"));
const auth_routes_1 = __importDefault(require("./auth/auth.routes"));
const restaurant_routes_1 = __importDefault(require("./restaurant/restaurant.routes"));
const branch_routes_1 = __importDefault(require("./branch/branch.routes"));
exports.default = { userRouter: user_routes_1.default, roleRouter: role_routes_1.default, authRouter: auth_routes_1.default, restaurantRouter: restaurant_routes_1.default, branchRouter: branch_routes_1.default };
//# sourceMappingURL=route-provider.js.map