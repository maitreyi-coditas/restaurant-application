"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_repo_1 = __importDefault(require("./user.repo"));
const user_responses_1 = require("./user.responses");
const create = (user) => {
    const userRecord = user_repo_1.default.create(user);
    console.log(userRecord);
    return userRecord;
};
const findOneAndUpdate = (filter, query) => user_repo_1.default.findOneAndUpdate(filter, query);
const findAll = () => user_repo_1.default.findAll();
const findOne = (filter) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_repo_1.default.findOne(filter);
    if (!user)
        throw user_responses_1.USER_RESPONSE.NOT_FOUND;
    return user;
});
const update = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const updatedUser = yield user_repo_1.default.update(user);
    if (updatedUser.modifiedCount > 0) {
        return user_responses_1.USER_RESPONSE.UPDATE_SUCCESSFULL;
        // return updatedUser
    }
    throw user_responses_1.USER_RESPONSE.UPDATE_FAILED;
});
const deleteOne = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const deletedUser = yield user_repo_1.default.deleteOne(id);
    if (deletedUser.modifiedCount > 0) {
        return user_responses_1.USER_RESPONSE.DELETE_SUCCESSFULL;
    }
    throw user_responses_1.USER_RESPONSE.DELETE_FAILED;
});
exports.default = {
    create,
    findAll,
    findOne,
    update,
    deleteOne,
    findOneAndUpdate
};
//# sourceMappingURL=user.services.js.map