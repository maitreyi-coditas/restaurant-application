"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const user_schema_1 = require("./user.schema");
const create = (user) => user_schema_1.UserModel.create(user);
const findAll = () => user_schema_1.UserModel.find({ isDeleted: false });
const findOne = (filter) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield user_schema_1.UserModel.findOne(Object.assign(Object.assign({}, filter), { isDeleted: false }));
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to find the user" };
    }
});
const update = (user) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield user_schema_1.UserModel.updateOne({ _id: new mongoose_1.Types.ObjectId(user._id) }, user);
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to update the user" };
    }
});
const findOneAndUpdate = (filter, query) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield user_schema_1.UserModel.findOneAndUpdate(filter, query);
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to find and update the user" };
    }
});
const deleteOne = (id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield user_schema_1.UserModel.updateOne({ _id: new mongoose_1.Types.ObjectId(id) }, { isDeleted: true });
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to delete the user" };
    }
});
exports.default = {
    create,
    findAll,
    findOne,
    update,
    findOneAndUpdate,
    deleteOne
};
//# sourceMappingURL=user.repo.js.map