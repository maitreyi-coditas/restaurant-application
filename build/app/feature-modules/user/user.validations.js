"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UPDATE_USER_VALIDATION = exports.CREATE_USER_VALIDATION = void 0;
const express_validator_1 = require("express-validator");
exports.CREATE_USER_VALIDATION = [
    (0, express_validator_1.body)("name").isString().notEmpty().withMessage("Name is required"),
    (0, express_validator_1.body)("email").isString().notEmpty().withMessage("Email is required"),
    // body("role").isObject.notEmpty().withMessage("Role is required"),
    (0, express_validator_1.body)("password").isString().notEmpty().isLength({ min: 6 }).withMessage("Password is required"),
    // body('restaurants').isArray().isLength({ min : 1})
];
exports.UPDATE_USER_VALIDATION = [
    (0, express_validator_1.body)("id").isString().notEmpty().withMessage("_id is required"),
    (0, express_validator_1.body)("name").optional().isString().notEmpty().withMessage("Name is required"),
    (0, express_validator_1.body)("email").optional().isString().notEmpty().withMessage("Email is required"),
    (0, express_validator_1.body)("password").optional().isString().notEmpty().isLength({ min: 6 }).withMessage("Password is required"),
    (0, express_validator_1.body)('restaurants').optional().isArray().isLength({ min: 1 })
];
//# sourceMappingURL=user.validations.js.map