"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOGIN_USER_VALIDATION = exports.CREATE_USER_VALIDATION = void 0;
const express_validator_1 = require("express-validator");
const validate_1 = require("../../utility/validate");
exports.CREATE_USER_VALIDATION = [
    (0, express_validator_1.body)("name").isString().notEmpty().withMessage("Name is required"),
    (0, express_validator_1.body)("email").isString().notEmpty().withMessage("Email is required"),
    (0, express_validator_1.body)("password").isString().notEmpty().isLength({ min: 6 }).withMessage("Password is required"),
    (0, express_validator_1.body)("role").optional(),
    (0, express_validator_1.body)('restaurants').optional(),
    validate_1.validate
];
exports.LOGIN_USER_VALIDATION = [
    (0, express_validator_1.body)("email").isString().notEmpty().withMessage("Email is required"),
    (0, express_validator_1.body)("password").isString().notEmpty().isLength({ min: 6 }).withMessage("Password is required"),
    validate_1.validate
];
//# sourceMappingURL=auth.validations.js.map