"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcryptjs_1 = require("bcryptjs");
const user_services_1 = __importDefault(require("../user/user.services"));
const role_types_1 = require("../roles/role.types");
const auth_responses_1 = require("./auth.responses");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const encryptUserPassword = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const salt = yield (0, bcryptjs_1.genSalt)(10);
    const hashedPassword = yield (0, bcryptjs_1.hash)(user.password, salt);
    user.password = hashedPassword;
    return user;
});
const register = (user) => __awaiter(void 0, void 0, void 0, function* () {
    user = yield encryptUserPassword(user);
    if (!user.role) {
        user.role = role_types_1.ROLE_IDS.CUSTOMER;
    }
    const record = yield user_services_1.default.create(user);
    return record;
});
const login = (credentials) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_services_1.default.findOne({ email: credentials.email });
    if (!user)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const isPasswordValid = yield (0, bcryptjs_1.compare)(credentials.password, user.password);
    if (!isPasswordValid)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const { _id, role } = user;
    const PRIVATE_KEY = fs_1.default.readFileSync(path_1.default.resolve(__dirname, "..\\..\\keys\\private.pem"), { encoding: "utf-8" });
    try {
        //payload, private key, algorithm
        var token = jsonwebtoken_1.default.sign({ id: _id, role: role }, PRIVATE_KEY || "", { algorithm: 'RS256' });
        return { token };
    }
    catch (error) {
        console.log(error);
    }
    // SYMMETRIC KEY
    // const { JWT_SECRET } = process.env;
    // const token = sign({id : _id, role : role}, JWT_SECRET || '')
    // console.log(token)
    // return { token };
});
exports.default = {
    register,
    login
};
//# sourceMappingURL=auth.services.js.map