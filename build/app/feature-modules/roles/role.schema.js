"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.roleModel = exports.roleSchema = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base-schema");
exports.roleSchema = new base_schema_1.BaseSchema({
    name: {
        type: String,
        required: true
    }
});
exports.roleModel = (0, mongoose_1.model)("Role", exports.roleSchema);
//# sourceMappingURL=role.schema.js.map