"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROLE = exports.ROLE_IDS = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
exports.ROLE_IDS = {
    ADMIN: new mongoose_1.default.mongo.ObjectId("6412e0ce774ea3db16abad3d"),
    CUSTOMER: new mongoose_1.default.mongo.ObjectId("6412e0da774ea3db16abad3f"),
    OWNER: new mongoose_1.default.mongo.ObjectId("64149fc552f92fa2d3afd525")
};
exports.ROLE = {
    ADMIN: exports.ROLE_IDS.ADMIN.toString(),
    CUSTOMER: exports.ROLE_IDS.CUSTOMER.toString(),
    OWNER: exports.ROLE_IDS.OWNER.toString()
};
// export type rolePredicate = (cb : IRole) => boolean
//# sourceMappingURL=role.types.js.map