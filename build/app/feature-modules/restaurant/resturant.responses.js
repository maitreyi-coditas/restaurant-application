"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RESTAURANT_RESPONSE = void 0;
exports.RESTAURANT_RESPONSE = {
    NOT_CREATED: {
        statusCode: 500,
        message: "Restaurant not Created"
    },
    NOT_FOUND: {
        statusCode: 404,
        message: "Restaurant Not Found"
    },
    NO_CONTENT: {
        statusCode: 204,
        message: "There are no restaurants right now"
    },
    UPDATE_FAILED: {
        statusCode: 400,
        message: "Restaurant not updated"
    },
    UPDATE_SUCCESSFULL: {
        statusCode: 200,
        message: "Restaurant updated succesfully"
    },
    DELETE_FAILED: {
        statusCode: 400,
        message: "Restaurant not deleted"
    },
    DELETE_SUCCESSFULL: {
        statusCode: 200,
        message: "Restaurant deleted succesfully"
    },
};
//# sourceMappingURL=resturant.responses.js.map