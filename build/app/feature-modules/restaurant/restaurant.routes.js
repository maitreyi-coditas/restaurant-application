"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const restaurant_services_1 = __importDefault(require("./restaurant.services"));
const response_handler_1 = require("../../utility/response.handler");
const permit_1 = require("../../middleware/permit");
const role_types_1 = require("../roles/role.types");
const router = (0, express_1.default)();
router.get('/', (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield restaurant_services_1.default.findAll();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.post('/create-restaurant', (0, permit_1.permit)([role_types_1.ROLE.CUSTOMER, role_types_1.ROLE.OWNER]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = res.locals.tokenDecode;
        const restaurant = req.body;
        const result = yield restaurant_services_1.default.create(restaurant, id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get('/display-status-pending-restaurants', (0, permit_1.permit)([role_types_1.ROLE.ADMIN]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield restaurant_services_1.default.findAllByStatus("pending");
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get('/display-status-approved-restaurants', (0, permit_1.permit)([role_types_1.ROLE.CUSTOMER, role_types_1.ROLE.OWNER]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield restaurant_services_1.default.findAllByStatus("approved");
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get('/display-my-restaurants', (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = res.locals.tokenDecode;
        const result = yield restaurant_services_1.default.findRestaurantsById(id);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.patch('/:restaurantId', (0, permit_1.permit)([role_types_1.ROLE.OWNER]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = res.locals.tokenDecode;
        const { restaurantId } = req.params;
        const update = req.body;
        const result = yield restaurant_services_1.default.update(id, { _id: restaurantId }, update);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.patch('/approve-restaurant/:restaurantId', (0, permit_1.permit)([role_types_1.ROLE.ADMIN]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { restaurantId } = req.params;
        const result = yield restaurant_services_1.default.updateStatus({ _id: restaurantId }, "approved");
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.patch('/reject-restaurant/:restaurantId', (0, permit_1.permit)([role_types_1.ROLE.ADMIN]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { restaurantId } = req.params;
        const result = yield restaurant_services_1.default.updateStatus({ _id: restaurantId }, "rejected");
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.delete('/:restaurantId', (0, permit_1.permit)([role_types_1.ROLE.ADMIN, role_types_1.ROLE.OWNER]), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = res.locals.tokenDecode;
        const { restaurantId } = req.params;
        const result = yield restaurant_services_1.default.deleteOne(id, { _id: restaurantId });
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
exports.default = router;
//# sourceMappingURL=restaurant.routes.js.map