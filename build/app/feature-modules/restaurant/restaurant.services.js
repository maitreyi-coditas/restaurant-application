"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const restaurant_repo_1 = __importDefault(require("./restaurant.repo"));
const restaurant_status_1 = require("./restaurant.status");
const resturant_responses_1 = require("./resturant.responses");
const user_responses_1 = require("../user/user.responses");
const user_services_1 = __importDefault(require("../user/user.services"));
const role_types_1 = require("../roles/role.types");
const create = (restaurant, id) => __awaiter(void 0, void 0, void 0, function* () {
    restaurant.status = restaurant_status_1.RESTAURANT_STATUS.PENDING;
    restaurant.ownerId = id;
    const newRestaurant = yield restaurant_repo_1.default.create(restaurant);
    if (!newRestaurant)
        throw resturant_responses_1.RESTAURANT_RESPONSE.NOT_CREATED;
    yield user_services_1.default.findOneAndUpdate({ _id: id }, { role: role_types_1.ROLE_IDS.OWNER });
    return newRestaurant;
});
const addBranch = (filter, update) => __awaiter(void 0, void 0, void 0, function* () {
    const updatedStatus = yield restaurant_repo_1.default.addBranch(filter, update);
    if (updatedStatus.modifiedCount > 0) {
        return resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL;
    }
    throw resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_FAILED;
});
const findAll = () => restaurant_repo_1.default.findAll();
const findOne = (filter) => restaurant_repo_1.default.findOne(filter);
const findAllByStatus = (statusState) => restaurant_repo_1.default.findAllByStatus(statusState);
//To find ALL the restaurants of ONE owner
const findRestaurantsById = (id) => restaurant_repo_1.default.findRestaurantsById(id);
const update = (id, filter, update) => __awaiter(void 0, void 0, void 0, function* () {
    const restaurant = yield restaurant_repo_1.default.findOne(filter);
    if ((restaurant === null || restaurant === void 0 ? void 0 : restaurant.ownerId.toString()) === id.toString()) {
        const updatedStatus = yield restaurant_repo_1.default.update(filter, update);
        if (updatedStatus.modifiedCount > 0) {
            return resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL;
        }
        throw resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_FAILED;
    }
    else {
        throw user_responses_1.USER_RESPONSE.UNAUTHORISED_ACCESS;
    }
});
const updateStatus = (filter, statusStr) => __awaiter(void 0, void 0, void 0, function* () {
    const restaurant = yield restaurant_repo_1.default.findOne(filter);
    if (!restaurant)
        throw resturant_responses_1.RESTAURANT_RESPONSE.NOT_FOUND;
    const ownerId = restaurant === null || restaurant === void 0 ? void 0 : restaurant.ownerId;
    const updatedStatus = yield restaurant_repo_1.default.updateStatus({ ownerId: ownerId }, { status: statusStr });
    if (updatedStatus.modifiedCount > 0) {
        return resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL;
    }
    else {
        throw resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_FAILED;
    }
});
const deleteOne = (id, filter) => __awaiter(void 0, void 0, void 0, function* () {
    const restaurant = yield restaurant_repo_1.default.findOne(filter);
    if ((restaurant === null || restaurant === void 0 ? void 0 : restaurant.ownerId.toString()) === id.toString()) {
        const updatedStatus = yield restaurant_repo_1.default.deleteOne(restaurant.id);
        if (updatedStatus.modifiedCount > 0) {
            return resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL;
        }
        throw resturant_responses_1.RESTAURANT_RESPONSE.UPDATE_FAILED;
    }
    else {
        throw user_responses_1.USER_RESPONSE.UNAUTHORISED_ACCESS;
    }
});
exports.default = {
    create,
    findAll,
    findAllByStatus,
    findOne,
    update,
    findRestaurantsById,
    addBranch,
    updateStatus,
    deleteOne
};
//# sourceMappingURL=restaurant.services.js.map