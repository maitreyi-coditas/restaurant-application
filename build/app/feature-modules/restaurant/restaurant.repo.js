"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const restaurant_schema_1 = require("./restaurant.schema");
const create = (restaurant) => restaurant_schema_1.RestaurantModel.create(restaurant);
const findAll = () => restaurant_schema_1.RestaurantModel.find();
const findAllByStatus = (statusState) => restaurant_schema_1.RestaurantModel.find({ status: statusState });
//To find all the restaurants of one owner
const findRestaurantsById = (id) => restaurant_schema_1.RestaurantModel.find({ ownerId: id });
const findOne = (filter) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield restaurant_schema_1.RestaurantModel.findOne(Object.assign(Object.assign({}, filter), { isDeleted: false }));
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to find the restaurant" };
    }
});
const update = (filter, update) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield restaurant_schema_1.RestaurantModel.updateOne(filter, update);
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to update the restaurant" };
    }
});
const deleteOne = (id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield restaurant_schema_1.RestaurantModel.updateOne({ _id: id }, { isDeleted: true });
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to delete the restaurant" };
    }
});
const addBranch = (filter, branch) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield restaurant_schema_1.RestaurantModel.updateOne(filter, { $push: { branches: branch } });
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to add the branch" };
    }
});
const updateStatus = (filter, status) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield restaurant_schema_1.RestaurantModel.updateMany(filter, status);
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to update the status of the restaurant" };
    }
});
const findByIdAndUpdate = (filter, update) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        console.log(filter, update);
        return yield restaurant_schema_1.RestaurantModel.findByIdAndUpdate(filter, update);
    }
    catch (e) {
        throw { message: "Something went wrong! Not able to update the status of the restaurant" };
    }
});
exports.default = {
    create,
    findAll,
    findAllByStatus,
    findOne,
    deleteOne,
    update,
    findRestaurantsById,
    addBranch,
    updateStatus,
    findByIdAndUpdate
};
//# sourceMappingURL=restaurant.repo.js.map