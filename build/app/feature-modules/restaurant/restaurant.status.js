"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RESTAURANT_STATUS = void 0;
exports.RESTAURANT_STATUS = {
    APPROVED: "approved",
    PENDING: "pending",
    REJECTED: "rejected"
};
//# sourceMappingURL=restaurant.status.js.map