"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantModel = void 0;
const mongoose_1 = require("mongoose");
const base_schema_1 = require("../../utility/base-schema");
const RestaurantSchema = new base_schema_1.BaseSchema({
    ownerId: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
    name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    branches: {
        type: [{
                _id: {
                    type: mongoose_1.Schema.Types.ObjectId,
                    required: true,
                },
                location: {
                    type: String,
                    required: true
                },
                menu: {
                    type: [{
                            name: {
                                type: String
                            },
                            price: {
                                type: String
                            }
                        }],
                    required: true
                },
                ownerId: {
                    type: mongoose_1.Schema.Types.ObjectId,
                    required: true,
                    ref: "User"
                },
                parentRestaurantId: {
                    type: mongoose_1.Schema.Types.ObjectId,
                    required: true,
                    ref: "Restaurant"
                }
            }],
        required: false
    },
    menu: {
        type: [{
                name: {
                    type: String
                },
                price: {
                    type: String
                }
            }],
        required: true
    },
    status: {
        type: String,
        required: true
    }
});
exports.RestaurantModel = (0, mongoose_1.model)("Restaurants", RestaurantSchema);
//# sourceMappingURL=restaurant.schema.js.map