"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.excludedPaths = exports.routes = void 0;
const routes_types_1 = require("./routes.types");
const route_provider_1 = __importDefault(require("../feature-modules/route-provider"));
const authorize_1 = require("../middleware/authorize");
exports.routes = [
    new routes_types_1.Routes('/role', route_provider_1.default.roleRouter),
    new routes_types_1.Routes('/auth', route_provider_1.default.authRouter),
    new routes_types_1.Routes('/users', route_provider_1.default.userRouter),
    new routes_types_1.Routes('/restaurant', route_provider_1.default.restaurantRouter),
    new routes_types_1.Routes('/branch', route_provider_1.default.branchRouter)
];
exports.excludedPaths = [
    new authorize_1.ExcludedPath("/auth/login", "POST")
];
//# sourceMappingURL=routes.data.js.map