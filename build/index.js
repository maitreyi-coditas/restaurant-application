"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
const app_1 = require("./app/app");
(0, app_1.startServer)();
// const populateDb = () => {
//     // roleServices.create({ _id : ROLE.ADMIN, name : "admin"})
//     // roleServices.create({ _id : ROLE.TRAINER, name : "trainer"})
//     for(let admin of adminList){
//      authServices.register(admin)
//     }
// }
// populateDb();
//# sourceMappingURL=index.js.map