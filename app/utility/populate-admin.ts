import { ROLE } from "../feature-modules/roles/role.types";


export const adminList = [

    {
        name: 'Administrator-1',
        email : "admin1@gmail.com",
        password : "adminPassword@1",
        role : ROLE.ADMIN
    },
    { 
        name: 'Administrator-2',
        email : "admin2@gmail.com",
        password : "adminPassword@2",
        role : ROLE.ADMIN
    },
    {
        name: 'Administrator-3',
        email : "admin3@gmail.com",
        password : "adminPassword@3",
        role : ROLE.ADMIN
    }

]
