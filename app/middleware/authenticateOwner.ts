import { Request,Response,NextFunction } from "express";
import restaurantServices from "../feature-modules/restaurant/restaurant.services";
import { USER_RESPONSE } from "../feature-modules/user/user.responses";

export const authenticateOwner = async(req:Request, res:Response, next:NextFunction) => {

    const {id} = res.locals.tokenDecode
    const {restaurantId} = req.params

    const restaurant = await restaurantServices.findOne({ _id : restaurantId})
   
    if(restaurant?.ownerId.toString() === id.toString()){
       return next()
    }
    throw USER_RESPONSE.UNAUTHORISED_ACCESS
}