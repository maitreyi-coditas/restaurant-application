import express from "express";
import { registerRoutes } from "./routes/routes";
import { connectToMongo } from "./connections/mongoose.connection";

export const startServer = async() =>  {

    try {

        const app = express();

        await connectToMongo()
    
        registerRoutes(app)    
    
        const { PORT } = process.env;
        
        app.listen(
            PORT || 1432,
            () => { console.log(`PORT STARTED ON PORT ${PORT || 1432}`)}
        )


    } catch(e){
        console.log("CANNOT START THE SERVER")
        console.log(e)
        process.exit(1)
    }

   
}