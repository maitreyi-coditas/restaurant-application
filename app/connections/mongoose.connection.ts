import {connect} from "mongoose";

export const connectToMongo = async() => {

    try {

        const { MONGO_CONNECTION } = process.env
        await connect(MONGO_CONNECTION || '')
        console.log('CONNECTED TO MONGODB')

        return true

    }catch(e){

        console.log('ERROR')
        throw { message : 'CANNOT CONNECT TO MONGODB'}

    }
    
}