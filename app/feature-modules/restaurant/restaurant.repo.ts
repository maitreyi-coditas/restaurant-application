import { Types } from "mongoose";
import { RestaurantModel } from "./restaurant.schema";
import { IRestaurant } from "./restaurant.types";
import { IBranch } from "../branch/branch.types";

const create = (restaurant : IRestaurant) => RestaurantModel.create(restaurant)

const findAll = () => RestaurantModel.find()

const findAllByStatus = (statusState : string) => RestaurantModel.find({status : statusState})

//To find all the restaurants of one owner
const findRestaurantsById = (id : string) => RestaurantModel.find({ ownerId : id})

const findOne = async(filter : Partial<IRestaurant>) => {

    try{

        return await RestaurantModel.findOne({
             ...filter,
             isDeleted : false
         })

     }catch(e){
         throw { message : "Something went wrong! Not able to find the restaurant"}
     }

}

const update = async(filter : Partial<IRestaurant>, update : Partial<IRestaurant>) => {
    try{
        return await RestaurantModel.updateOne(filter, update)
    }catch(e){
        throw { message : "Something went wrong! Not able to update the restaurant"}
    }
} 

const deleteOne = async(id : string) => {
    try {
        return await RestaurantModel.updateOne({ _id : id}, {isDeleted: true});
    }catch(e){
        throw { message : "Something went wrong! Not able to delete the restaurant"}
    }
} 

const addBranch = async(filter : Partial<IRestaurant>, branch : IBranch) => {

    try{
        return await RestaurantModel.updateOne(filter, 
        {$push: {branches: branch}}
    );
    }catch(e){
        throw { message : "Something went wrong! Not able to add the branch"}
    }

} 

const updateStatus = async(filter : Partial<IRestaurant>, status : Partial<IRestaurant>) => {

    try{
        return await RestaurantModel.updateMany(filter,status)
    }catch(e){
        throw { message : "Something went wrong! Not able to update the status of the restaurant"}
    }
}

const findByIdAndUpdate = async(filter :any, update: any) => {
    try{
        console.log(filter,update)
        return await RestaurantModel.findByIdAndUpdate(filter,update)
    }catch(e){
        throw { message : "Something went wrong! Not able to update the status of the restaurant"}
    }

}


export default{
    create,
    findAll,
    findAllByStatus,
    findOne,
    deleteOne,
    update,
    findRestaurantsById,
    addBranch,
    updateStatus,
    findByIdAndUpdate
}