import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IRestaurant } from "./restaurant.types";

const RestaurantSchema = new BaseSchema({

    ownerId : {
        type : Schema.Types.ObjectId,
        required:true,
        ref:"User"

    },
    name : {
        type : String,
        required:true
    },
   
    location: {
        type:String,
        required:true
    },

    branches: {
        type:[{
            _id : {
                type : Schema.Types.ObjectId,
                required:true,
            },
            location: {
                type:String,
                required:true
            },
            menu: {
                type:[{
                    name: {
                        type: String
                    },
                    price : {
                        type: String
                    }
                }],
                required: true
            },
            ownerId : {
                type : Schema.Types.ObjectId,
                required:true,
                ref:"User"
            },
            parentRestaurantId : {
                type : Schema.Types.ObjectId,
                required:true,
                ref:"Restaurant"
            }
        }],
        required:false
    },


    menu: {
        type:[{
            name: {
                type: String
            },
            price : {
                type: String
            }
        }],
        required: true
    },

    status : {
        type : String,
        required : true
    }


})

type RestaurantDocument = Document & IRestaurant

export const RestaurantModel = model<RestaurantDocument>("Restaurants",RestaurantSchema);