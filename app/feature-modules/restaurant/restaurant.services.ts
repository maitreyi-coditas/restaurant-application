import restaurantRepo from "./restaurant.repo";
import { RESTAURANT_STATUS } from "./restaurant.status";
import { IRestaurant } from "./restaurant.types";
import { RESTAURANT_RESPONSE } from "./resturant.responses";
import { USER_RESPONSE } from "../user/user.responses";
import userServices from "../user/user.services";
import { ROLE_IDS } from "../roles/role.types";
import { IBranch } from "../branch/branch.types";

const create = async(restaurant : IRestaurant, id : string) => {

    restaurant.status = RESTAURANT_STATUS.PENDING
    restaurant.ownerId = id

    const newRestaurant = await restaurantRepo.create(restaurant)
    if(!newRestaurant) throw RESTAURANT_RESPONSE.NOT_CREATED

    return newRestaurant
}

const addBranch = async(filter : Partial<IRestaurant>, update : IBranch) => {

    const updatedStatus = await restaurantRepo.addBranch(filter,update)
    if(updatedStatus.modifiedCount>0){
        return RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL
    }
    throw RESTAURANT_RESPONSE.UPDATE_FAILED
}

const findAll  = () => restaurantRepo.findAll()

const findOne = (filter: Partial<IRestaurant>) => restaurantRepo.findOne(filter);

const findAllByStatus  = (statusState : string) => restaurantRepo.findAllByStatus(statusState)

//To find ALL the restaurants of ONE owner
const findRestaurantsById = (id : string) => restaurantRepo.findRestaurantsById(id)

const update = async(id : string, filter : Partial<IRestaurant>, update : Partial<IRestaurant>) => {

        const restaurant = await restaurantRepo.findOne(filter)
    
        if(restaurant?.ownerId.toString() === id.toString()){
            const updatedStatus = await restaurantRepo.update(filter,update)
            if(updatedStatus.modifiedCount>0){
                return RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL
            }
            throw RESTAURANT_RESPONSE.UPDATE_FAILED
        }else{
            throw USER_RESPONSE.UNAUTHORISED_ACCESS
        }
};

const updateStatus = async(filter : Partial<IRestaurant>, statusStr : string) => {

    const restaurant = await restaurantRepo.findOne(filter)
    if(!restaurant) throw RESTAURANT_RESPONSE.NOT_FOUND

    const ownerId = restaurant?.ownerId
    const updatedStatus = await restaurantRepo.updateStatus({ ownerId : ownerId} , {status : statusStr})
  
    if(updatedStatus.modifiedCount>0)
    {
        return RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL
    } 
    else{
        throw RESTAURANT_RESPONSE.UPDATE_FAILED
    }
}

const deleteOne = async(id: string, filter : Partial<IRestaurant>) => {

    const restaurant = await restaurantRepo.findOne(filter)
    
    if(restaurant?.ownerId.toString() === id.toString()){
        const updatedStatus = await restaurantRepo.deleteOne(restaurant.id)
        if(updatedStatus.modifiedCount>0){
            return RESTAURANT_RESPONSE.UPDATE_SUCCESSFULL
        }
        throw RESTAURANT_RESPONSE.UPDATE_FAILED
    }else{
        throw USER_RESPONSE.UNAUTHORISED_ACCESS
    }

}


export default {
    create,
    findAll,
    findAllByStatus,
    findOne,
    update,
    findRestaurantsById,
    addBranch,
    updateStatus,
    deleteOne
}