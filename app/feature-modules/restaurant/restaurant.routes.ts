import Router, { Request,Response,NextFunction } from "express";
import restaurantServices from "./restaurant.services";
import { ResponseHandler } from "../../utility/response.handler";
import { permit } from "../../middleware/permit";
import { ROLE } from "../roles/role.types";

const router = Router();

router.get('/',async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const result = await restaurantServices.findAll()
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.post('/create-restaurant', permit([ROLE.CUSTOMER,ROLE.OWNER]),async(req:Request,res:Response,next:NextFunction)=>{

    try{
        const {id} = res.locals.tokenDecode;
        const restaurant = req.body
        const result = await restaurantServices.create(restaurant,id)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.get('/display-status-pending-restaurants', permit([ROLE.ADMIN]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const result = await restaurantServices.findAllByStatus("pending")
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.get('/display-status-approved-restaurants', permit([ROLE.CUSTOMER,ROLE.OWNER]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const result = await restaurantServices.findAllByStatus("approved")
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.get('/display-my-restaurants', async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const { id } = res.locals.tokenDecode;
        const result = await restaurantServices.findRestaurantsById(id)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.patch('/:restaurantId',permit([ROLE.OWNER]), async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const { id } = res.locals.tokenDecode;
        const { restaurantId } = req.params;
        const update = req.body
        const result = await restaurantServices.update(id,{ _id : restaurantId},update)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.patch('/approve-restaurant/:restaurantId',permit([ROLE.ADMIN]), async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const { restaurantId } = req.params;    
        const result = await restaurantServices.updateStatus({ _id : restaurantId},"approved")
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.patch('/reject-restaurant/:restaurantId',permit([ROLE.ADMIN]), async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const { restaurantId } = req.params;    
        const result = await restaurantServices.updateStatus({ _id : restaurantId},"rejected")
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.delete('/:restaurantId',permit([ROLE.ADMIN,ROLE.OWNER]),async(req:Request,res:Response,next:NextFunction)=>{
    try{
        const { id } = res.locals.tokenDecode;
        const { restaurantId } = req.params;   
        const result = await restaurantServices.deleteOne(id,{ _id : restaurantId})
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

export default router