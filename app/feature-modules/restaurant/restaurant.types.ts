import { ObjectId } from "bson";
import { IBranch } from "../branch/branch.types";

export interface IRestaurant{

    _id?:string,
    name:string,
    location: string,
    menu : Menu[],
    branches? : IBranch[],
    status :string,
    ownerId : string,   
}

export interface IMenu{

    name : string,
    price: string

}

export type Menu = IMenu[]