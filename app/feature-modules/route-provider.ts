import userRouter from "./user/user.routes"
import roleRouter from "./roles/role.routes"
import authRouter from "./auth/auth.routes"
import restaurantRouter from "./restaurant/restaurant.routes"
import branchRouter from "./branch/branch.routes"

export default { userRouter, roleRouter,authRouter,restaurantRouter,branchRouter}