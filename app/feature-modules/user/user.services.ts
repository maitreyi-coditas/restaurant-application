import { IUser } from "./user.types";
import userRepo from "./user.repo";
import { USER_RESPONSE } from "./user.responses";
import { IRestaurant } from "../restaurant/restaurant.types";
import restaurantServices from "../restaurant/restaurant.services";
import { RESTAURANT_RESPONSE } from "../restaurant/resturant.responses";
import { RESTAURANT_STATUS } from "../restaurant/restaurant.status";
import { ObjectId } from "bson";
import { ROLE, ROLE_IDS } from "../roles/role.types";

const create = (user : IUser) => {

    const userRecord = userRepo.create(user)
    console.log(userRecord)

    return userRecord
}

const findOneAndUpdate = (filter : Partial<IUser>,query : Partial<IUser>) => userRepo.findOneAndUpdate(filter,query)

const findAll  = () => userRepo.findAll()

const findOne = async(filter: Partial<IUser>) => {

    const user = await userRepo.findOne(filter);

    if(!user) throw USER_RESPONSE.NOT_FOUND

    return user
};


const update = async(user : IUser) => {
    
    const updatedUser = await userRepo.update(user)
    if(updatedUser.modifiedCount>0){
        return USER_RESPONSE.UPDATE_SUCCESSFULL
        // return updatedUser
    }
    throw USER_RESPONSE.UPDATE_FAILED


};

const deleteOne = async(id : string) => {

    const deletedUser = await userRepo.deleteOne(id)

    if(deletedUser.modifiedCount>0){
        return USER_RESPONSE.DELETE_SUCCESSFULL
    }
    throw USER_RESPONSE.DELETE_FAILED
}


export default{
    create,
    findAll,
    findOne,
    update,
    deleteOne,
    findOneAndUpdate
}

