import { NextFunction, Router,Response,Request } from "express";
import userServices from "./user.services";
import { ResponseHandler } from "../../utility/response.handler";

export const router= Router();

router.get("/",async(req:Request,res:Response,next:NextFunction) => {
    try{
        const result = await userServices.findAll();

        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
});

router.get("/:id", async(req,res,next) => {
    try{ 
        
        const {id} = req.params;
        const result = await userServices.findOne({_id : id});
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
});

router.put("/", async(req : Request, res : Response, next : NextFunction) => {
    try{ 
        const user = req.body;
        const result = await userServices.update(user);
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
})

router.delete("/:id", async(req : Request, res : Response, next : NextFunction) => {
    try{ 
        const { id } = req.params;
        const result = await userServices.deleteOne(id);
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
})

export default router;