import {NextFunction,Request,Response,Router} from "express"
import { permit } from "../../middleware/permit"
import { ROLE } from "../roles/role.types"
import branchServices from "./branch.services"
import { ResponseHandler } from "../../utility/response.handler"

const router = Router()

router.post('/create-branch/:parentRestaurantId',permit([ROLE.OWNER]), async(req:Request, res:Response, next:NextFunction) => {
   try{

    const {parentRestaurantId} = req.params
    const {id} = res.locals.tokenDecode;
    const branch = req.body

    const result = await branchServices.create(parentRestaurantId,id,branch)

    res.send(new ResponseHandler(result))
   }catch(e){
    next(e)
   }
})

router.get('/',async(req:Request, res:Response, next:NextFunction) =>{
    try{
        const result = await branchServices.findAll()

        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

export default router