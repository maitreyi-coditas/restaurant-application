
import restaurantServices from "../restaurant/restaurant.services"
import { RESTAURANT_RESPONSE } from "../restaurant/resturant.responses"
import { USER_RESPONSE } from "../user/user.responses"
import branchRepo from "./branch.repo"
import { BRANCH_RESPONSE } from "./branch.responses"
import { IBranch } from "./branch.types"

const create = async(restaurantId : string, id : string, branch: IBranch) => {

    const restaurant = await restaurantServices.findOne({ _id : restaurantId})
    if(!restaurant) throw RESTAURANT_RESPONSE.NOT_FOUND

    if(restaurant?.ownerId.toString() === id.toString()){
        
        branch.ownerId = restaurant.ownerId
        branch.parentRestaurantId = restaurant._id

        const newBranch = await branchRepo.create(branch)
        if(!newBranch) throw BRANCH_RESPONSE.NOT_CREATED

        const updatedRestaurantResponse = await restaurantServices.addBranch({ _id : restaurant._id},newBranch)
        return updatedRestaurantResponse

    }else{
        throw USER_RESPONSE.UNAUTHORISED_ACCESS
    }
}

const findAll = () => branchRepo.findAll()

const updateStatus = async(filter : Partial<IBranch>, status : Partial<IBranch>) => branchRepo.updateStatus(filter,status)

export default {
    create,
    findAll,
    updateStatus
}