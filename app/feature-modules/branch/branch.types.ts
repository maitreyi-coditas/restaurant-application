import { ObjectId } from "bson";
import { Menu } from "../restaurant/restaurant.types";

export interface IBranch {

    _id?:string,
    location: string,
    menu : Menu[],
    ownerId : string,   
    parentRestaurantId : string
}