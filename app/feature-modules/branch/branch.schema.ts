import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IBranch } from "./branch.types";

const branchSchema = new BaseSchema ({
    
    location: {
        type:String,
        required:true
    },
    menu: {
        type:[
            {
            name: {
                type: String
            },
            price : {
                type: String
            }
        }
    ],
        required: true
    },

    ownerId : {
        type : Schema.Types.ObjectId,
        required:true,
        ref:"User"
    },

    parentRestaurantId : {
        type : Schema.Types.ObjectId,
        required:true,
        ref:"Restaurant"
    }
  
})

type BranchDocument = Document & IBranch

export const BranchModel = model<BranchDocument>("BranchRestaurants",branchSchema);