import { BranchModel } from "./branch.schema";
import { IBranch } from "./branch.types";

const create = (branch : IBranch) => BranchModel.create(branch)

const findAll = () => BranchModel.find()

const updateStatus = async(filter : Partial<IBranch>, status : Partial<IBranch>) => {
    try{
        return await BranchModel.updateMany(filter,status)
    }
    catch(e){
        throw { message : "Something went wrong! Not able to update the status of the restaurant"}
    }
}

export default {
    create,
    findAll,
    updateStatus
}