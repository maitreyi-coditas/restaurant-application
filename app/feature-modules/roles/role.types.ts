import  mongoose  from "mongoose";
import { ObjectId } from "bson";

export interface IRole {

    _id? : ObjectId,
    name : string
}

export type Role = IRole[]    

export const ROLE_IDS = {
    ADMIN : new mongoose.mongo.ObjectId("6412e0ce774ea3db16abad3d"),
    CUSTOMER : new mongoose.mongo.ObjectId("6412e0da774ea3db16abad3f"),
    OWNER : new mongoose.mongo.ObjectId("64149fc552f92fa2d3afd525")
}

export const ROLE = {
    ADMIN: ROLE_IDS.ADMIN.toString(),
    CUSTOMER: ROLE_IDS.CUSTOMER.toString(),
    OWNER: ROLE_IDS.OWNER.toString()
}

// export type rolePredicate = (cb : IRole) => boolean