import { roleModel } from "./role.schema";
import { IRole } from "./role.types";

const create = (role : IRole) => roleModel.create(role);

export default{
    create
}