import { Router } from "express";
import roleServices from "./role.services";
import { ResponseHandler } from "../../utility/response.handler";

const router = Router()

router.post("/add", async (req,res,next) => {

    try{
        const result = await roleServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
    
})

export default router;