import { body } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_USER_VALIDATION = [
    body("name").isString().notEmpty().withMessage("Name is required"),
    body("email").isString().notEmpty().withMessage("Email is required"),
    body("password").isString().notEmpty().isLength({ min : 6}).withMessage("Password is required"),
    body("role").optional(),
    body('restaurants').optional(),
    validate
]

export const LOGIN_USER_VALIDATION = [
    body("email").isString().notEmpty().withMessage("Email is required"),
    body("password").isString().notEmpty().isLength({ min : 6}).withMessage("Password is required"),
    validate
]