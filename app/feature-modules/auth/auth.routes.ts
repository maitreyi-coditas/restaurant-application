import Router, { Request,Response,NextFunction } from "express";
import { ResponseHandler } from "../../utility/response.handler";
import authServices from "./auth.services";
import { CREATE_USER_VALIDATION, LOGIN_USER_VALIDATION } from "./auth.validations";

const router = Router();

router.post("/register", CREATE_USER_VALIDATION,async(req : Request, res : Response, next : NextFunction) => {

    try{ 
        const user = req.body;
        const result = await authServices.register(user);
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
})

router.post("/login", LOGIN_USER_VALIDATION, async(req : Request, res : Response, next : NextFunction)=>{

    try{ 
        const credentials = req.body;
        const result = await authServices.login(credentials);
        res.send(new ResponseHandler(result));
    }catch(e){
        next(e)
    }
})

export default router;