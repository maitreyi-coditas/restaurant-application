import { Route, Routes } from "./routes.types";
import routeProvider from "../feature-modules/route-provider";
import { ExcludedPath, ExcludedPaths } from "../middleware/authorize";

export const routes : Route = [

    new Routes('/role',routeProvider.roleRouter),
    new Routes('/auth',routeProvider.authRouter),
    new Routes('/users',routeProvider.userRouter),
    new Routes('/restaurant',routeProvider.restaurantRouter),
    new Routes('/branch',routeProvider.branchRouter)
    
]


export const excludedPaths: ExcludedPaths = [
    new ExcludedPath("/auth/login", "POST"),
    new ExcludedPath('auht/register',"POST"),
    new ExcludedPath('/restaurants',"GET"),

];
